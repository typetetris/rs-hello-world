{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";
      in
      rec {
        packages.rs-hello-world = naersk-lib.buildPackage {
          pname = "rs-hello-world";
          root = ./.;
        };
        defaultPackage = packages.rs-hello-world;

        # `nix run`
        apps.rs-hello-world = utils.lib.mkApp {
          drv = packages.rs-hello-world;
        };
        defaultApp = apps.rs-hello-world;

        # `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [ rustc cargo ];
        };
      });
}
